/**
 * 
 */
package me.hww.slidemenu.model;

/**
 * @author HuangWenwei
 * 
 * @date 2014年8月5日
 */
public class NavDrawerItem {
	private String title;
	private int icon;
	private String count = "0";
	// counter是否可见
	private boolean isCounterVisible = false;

	public NavDrawerItem() {
	}

	/**
	 * @param title
	 * @param icon
	 */
	public NavDrawerItem(String title, int icon) {
		super();
		this.title = title;
		this.icon = icon;
	}

	/**
	 * @param title
	 * @param icon
	 * @param count
	 * @param isCounterVisible
	 */
	public NavDrawerItem(String title, int icon, boolean isCounterVisible,
			String count) {
		this.title = title;
		this.icon = icon;
		this.isCounterVisible = isCounterVisible;
		this.count = count;

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public boolean isCounterVisible() {
		return isCounterVisible;
	}

	public void setCounterVisible(boolean isCounterVisible) {
		this.isCounterVisible = isCounterVisible;
	}
}
